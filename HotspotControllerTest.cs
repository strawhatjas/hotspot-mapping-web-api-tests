using System;
using Xunit;
using hotspot_mapping_web_api;
using hotspot_mapping_web_api.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using hotspot_mapping_web_api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using hotspot_mapping_web_api.Data;
using hotspot_mapping_web_api.Services;
using hotspot_mapping_web_api.DTOs;
using NetTopologySuite.Geometries;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace hotspot_mapping_web_api_tests
{
    public class HotspotControllerTest
    {
        private DbContextOptions<HotspotDbContext> dbContextOptions = new DbContextOptionsBuilder<HotspotDbContext>()
            .UseInMemoryDatabase(databaseName: "testDb")
            .Options;

        public HotspotControllerTest()
        {
            //auto mapper configuration
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            Mapper = mockMapper.CreateMapper();
            MockService = new Mock<HotspotService>();

            Seed();
        }

        protected IMapper Mapper { get; }
        protected Mock<HotspotService> MockService { get; }

        private void Seed()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var one = new Hotspot
                {
                    Id = 1,
                    SiteName = "Alberton Caf�",
                    Address = "198 Bridport St",
                    SuburbName = "Albert Park",
                    Postcode = "3206",
                    State = "VIC",
                    Country = "Australia",
                    Latitude = -37.841030,
                    Longitude = 144.953770,
                    GeoCoordinate = new Point(144.953770, -37.841030) { SRID = 4326 },
                    StartDate = DateTime.ParseExact("2021-04-01 08:50", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                    EndDate = DateTime.ParseExact("2021-04-01 10:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                    Note = "Case dined at venue"
                };

                context.AddRange(one);

                context.SaveChanges();
            }
        }

        private static T GetObjectResultContent<T>(ActionResult<T> result)
        {
            return (T)((ObjectResult)result.Result).Value;
        }

        [Fact]
        public async Task GetHotspot()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);

                // Act
                var actionResult = await controller.Get(1);

                // Assert
                var result = actionResult.Result as OkObjectResult;
                var resultObject = GetObjectResultContent<Hotspot>(actionResult);

                Assert.NotNull(result);
                Assert.Equal(200, result.StatusCode);
                Assert.Equal(1, resultObject.Id);
                Assert.Equal("Alberton Caf�", resultObject.SiteName);
                Assert.Equal("198 Bridport St", resultObject.Address);
                Assert.Equal("Albert Park", resultObject.SuburbName);
                Assert.Equal("3206", resultObject.Postcode);
                Assert.Equal("VIC", resultObject.State);
                Assert.Equal("Australia", resultObject.Country);
                Assert.Equal(-37.841030, resultObject.Latitude);
                Assert.Equal(144.953770, resultObject.Longitude);
                Assert.Equal(DateTime.ParseExact("2021-04-01 08:50", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture), resultObject.StartDate);
                Assert.Equal(DateTime.ParseExact("2021-04-01 10:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture), resultObject.EndDate);
                Assert.Equal("Case dined at venue", resultObject.Note);
            }
        }

        [Fact]
        public async Task AddHotspot()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);
                var newHotspotDTO = new HotspotDTO
                {
                    SiteName = "North Point Cafe",
                    Address = "2B North Rd",
                    SuburbName = "Brighton",
                    Postcode = "3186",
                    StartDate = DateTime.ParseExact("2021-05-01 08:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                    EndDate = DateTime.ParseExact("2021-05-01 09:30", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                    Note = "Case dined outside and used bathroom"
                };

                // Act
                var actionResult = await controller.Post(newHotspotDTO);

                // Assert
                var result = actionResult.Result as OkObjectResult;
                var resultObject = GetObjectResultContent<Hotspot>(actionResult);

                Assert.NotNull(result);
                Assert.Equal(200, result.StatusCode);
                Assert.Equal("North Point Cafe", resultObject.SiteName);
                Assert.Equal("2B North Rd", resultObject.Address);
                Assert.Equal("Brighton", resultObject.SuburbName);
                Assert.Equal("3186", resultObject.Postcode);
                Assert.Equal("VIC", resultObject.State);
                Assert.Equal("Australia", resultObject.Country);
                Assert.Equal(-37.8977581, resultObject.Latitude);
                Assert.Equal(144.9864129, resultObject.Longitude);
                Assert.Equal(DateTime.ParseExact("2021-05-01 08:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture), resultObject.StartDate);
                Assert.Equal(DateTime.ParseExact("2021-05-01 09:30", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture), resultObject.EndDate);
                Assert.Equal("Case dined outside and used bathroom", resultObject.Note);
            }
        }

        [Fact]
        public async Task UpdateHotspot()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);
                var newHotspotDTO = new HotspotDTO
                {
                    Id = 1,
                    SiteName = "North Point Cafe",
                    Address = "2B North Rd",
                    SuburbName = "Brighton",
                    Postcode = "3186",
                    StartDate = DateTime.ParseExact("2021-05-02 08:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                    EndDate = DateTime.ParseExact("2021-05-02 09:30", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                    Note = "Case dined outside and used bathroom"
                };

                // Act
                var actionResult = await controller.Put(1, newHotspotDTO);

                // Assert
                var result = actionResult.Result as OkObjectResult;
                var resultObject = GetObjectResultContent<Hotspot>(actionResult);

                Assert.NotNull(result);
                Assert.Equal(200, result.StatusCode);
                Assert.Equal("North Point Cafe", resultObject.SiteName);
                Assert.Equal("2B North Rd", resultObject.Address);
                Assert.Equal("Brighton", resultObject.SuburbName);
                Assert.Equal("3186", resultObject.Postcode);
                Assert.Equal("VIC", resultObject.State);
                Assert.Equal("Australia", resultObject.Country);
                Assert.Equal(-37.8977581, resultObject.Latitude);
                Assert.Equal(144.9864129, resultObject.Longitude);
                Assert.Equal(DateTime.ParseExact("2021-05-02 08:10", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture), resultObject.StartDate);
                Assert.Equal(DateTime.ParseExact("2021-05-02 09:30", "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture), resultObject.EndDate);
                Assert.Equal("Case dined outside and used bathroom", resultObject.Note);
            }
        }

        [Fact]
        public async Task DeleteHotspot()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);

                // Act
                var actionResult = await controller.Delete(1);
                var validateResult = await controller.Get(1);

                // Assert
                Assert.IsType<OkResult>(actionResult);
                Assert.IsType<BadRequestObjectResult>(validateResult.Result);
            }
        }

        [Fact]
        public async Task ListHotspotByDistanceGeolocationAndDate()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);

                // Act
                var actionResult = await controller.GetAllWithinRange(3000, -37.841030, 144.953770, DateTime.ParseExact("2021-03-30", "yyyy-MM-dd", CultureInfo.InvariantCulture), DateTime.ParseExact("2021-04-03", "yyyy-MM-dd", CultureInfo.InvariantCulture));

                // Assert
                var result = actionResult.Result as OkObjectResult;
                var resultObject = GetObjectResultContent<IEnumerable<Hotspot>>(actionResult);

                Assert.NotNull(result);
                // Expects that the returned list only has one object
                Assert.Single(resultObject);

            }
        }

        [Fact]
        public async Task GetHotspotList()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);

                // Act
                var actionResult = await controller.GetAll();

                // Assert
                var result = actionResult.Result as OkObjectResult;
                var resultObject = GetObjectResultContent<IEnumerable<Hotspot>>(actionResult);

                Assert.NotNull(result);
                // Expects that the returned list only has one object
                Assert.Single(resultObject);
            }
        }

        [Fact]
        public async Task GetHotspotListByDateRange()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);

                // Act
                var actionResult = await controller.GetByDateRange(DateTime.ParseExact("2021-03-30", "yyyy-MM-dd", CultureInfo.InvariantCulture), DateTime.ParseExact("2021-04-03", "yyyy-MM-dd", CultureInfo.InvariantCulture));

                // Assert
                var result = actionResult.Result as OkObjectResult;
                var resultObject = GetObjectResultContent<IEnumerable<Hotspot>>(actionResult);

                Assert.NotNull(result);
                // Expects that the returned list only has one object
                Assert.Single(resultObject);

            }
        }

        [Fact]
        public async Task RefreshHotspot()
        {
            using (var context = new HotspotDbContext(dbContextOptions))
            {
                // Arrange
                var controller = new HotspotController(context, MockService.Object, Mapper);

                // Act
                var actionResult = await controller.Refresh();

                // Assert
                Assert.IsType<OkResult>(actionResult);
            }
        }
    }
}
