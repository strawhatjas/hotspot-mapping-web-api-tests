﻿using hotspot_mapping_web_api.Controllers;
using hotspot_mapping_web_api.Data;
using hotspot_mapping_web_api.DTOs;
using hotspot_mapping_web_api.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace hotspot_mapping_web_api_tests
{
    public class AccountControllerTest
    {
        public AccountControllerTest()
        {
            var inMemorySettings = new Dictionary<string, string> {
                { "Tokens:Key", "adjas@$sds&&*(@@#$?>;;0adasaddFDA&^3ll;sada62;;;2%%^#$%^2" },
                { "Tokens:Issuer", "localhost" },
                { "Tokens:Audience", "localhost" }
            };

            Config = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();
        }

        protected IConfiguration Config { get; }

        [Fact]
        public async Task Login()
        {
            // Arrange
            var userManager = new Mock<UserManager<User>>(
                            /* IUserStore<TUser> store */Mock.Of<IUserStore<User>>(),
                            /* IOptions<IdentityOptions> optionsAccessor */null,
                            /* IPasswordHasher<TUser> passwordHasher */null,
                            /* IEnumerable<IUserValidator<TUser>> userValidators */null,
                            /* IEnumerable<IPasswordValidator<TUser>> passwordValidators */null,
                            /* ILookupNormalizer keyNormalizer */null,
                            /* IdentityErrorDescriber errors */null,
                            /* IServiceProvider services */null,
                            /* ILogger<UserManager<TUser>> logger */null);

            var signInManager = new Mock<SignInManager<User>>(
                            userManager.Object,
                            /* IHttpContextAccessor contextAccessor */Mock.Of<IHttpContextAccessor>(),
                            /* IUserClaimsPrincipalFactory<TUser> claimsFactory */Mock.Of<IUserClaimsPrincipalFactory<User>>(),
                            /* IOptions<IdentityOptions> optionsAccessor */null,
                            /* ILogger<SignInManager<TUser>> logger */null,
                            /* IAuthenticationSchemeProvider schemes */null,
                            /* IUserConfirmation<TUser> confirmation */null);

            var user = new User
            {
                UserName = "Admin",
                Id = Guid.NewGuid().ToString(),
                Email = "admin@test.com",
                FirstName = "Admin"
            };

            userManager.Setup(x => x.CreateAsync(user, It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            userManager.Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(user);

            signInManager.Setup(x => x.CheckPasswordSignInAsync(user, It.IsAny<string>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Success);

            var controller = new AccountController(signInManager.Object, userManager.Object, Config);

            // Act
            var actionResult = await controller.Login(new LoginDTO { Email = "admin@test.com", Password = "password" });

            // Assert
            var result = actionResult as OkObjectResult;

            Assert.IsType<OkObjectResult>(actionResult);
            Assert.NotNull(result.Value);
        }
    }
}
